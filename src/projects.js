import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

export const projectsRouter = express.Router();

projectsRouter.use(cors());
projectsRouter.use(bodyParser.urlencoded({ extended: true }));
projectsRouter.use(bodyParser.json());

projectsRouter.post('/', async (req, res) => {
    const db = await setupDB();
    const { name } = req.body;

    const slug = name.toLowerCase();

    const project = {
        slug,
        boards: [],
    };

    const existingProject = await db.collection('projects').findOne({ slug });

    if (existingProject) {
        res.status(400).json({ error: `project ${slug} already exists` });
    } else {
        await db.collection('projects').insertOne(project);
        res.json({ project });
    }
});

const findProject = async (req, res, next) => {
    const db = await setupDB();

    const { slug } = req.params;

    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
        res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
        req.body.project = project;
        next();
    }
};

projectsRouter.get('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
});

projectsRouter.delete('/', async (req, res) => {
    const db = await setupDB();

    const { name } = req.body;
    const slug = name.toLowerCase();

    const r = await db.collection('projects').deleteOne({ slug });
    if (r.deletedCount == 0) {
        res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
        res.status(200).send('deleted project');
    }
});

projectsRouter.post('/:slug/boards', findProject, async (req, res) => {
    const db = await setupDB();
    
    const { name, project } = req.body;
    const board = { name, tasks: [] };
    project.boards.push(board);

    await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
});

projectsRouter.delete('/:slug/boards', findProject, async (req, res) => {
    const db = await setupDB();

    const { name, project } = req.body;
    project.boards = project.boards.filter(board => board['name'] != name);

    await db.collection('projects').findOneAndReplace({ slug: project.slug }, project);

    res.status(200).send('deleted board');
});

projectsRouter.post('/:slug/boards/:name/tasks', findProject, async (req, res) => {
    const db = await setupDB();
    
    const { name, project } = req.body;
    const boardName = req.params.name;
    if (!(project.boards.some(el => el.name == boardName))) {
        res.status(400).json({ error: `board ${boardName} does not exist` });
    } else {
        // Find board and push task
        const board = project.boards.find((o, i) => {
            if (o.name == boardName) {
                project.boards[i].tasks.push(name);
                return true;  // stop searching
            }
        });

        await db
            .collection('projects')
            .findOneAndReplace({ slug: project.slug }, project);
        
        res.json({ board });
    }
});

projectsRouter.delete('/:slug/boards', findProject, async (req, res) => {
    const db = await setupDB();

    const { name, project } = req.body;
    project.boards = project.boards.filter(board => board['name'] != name);

    await db.collection('projects').findOneAndReplace({ slug: project.slug }, project);

    res.status(200).send('deleted board');
});

projectsRouter.delete('/:slug/boards/:name/tasks', findProject, async (req, res) => {
    const db = await setupDB();
    
    const { name, project } = req.body;
    const boardName = req.params.name;
    if (!(project.boards.some(el => el.name == boardName))) {
        res.status(400).json({ error: `board ${boardName} does not exist` });
    } else {
        // Find board and remove task
        const board = project.boards.find((o, i) => {
            if (o.name == boardName) {
                const index = project.boards[i].tasks.indexOf(name);
                if (index > -1) {
                    project.boards[i].tasks.splice(index, 1);
                }
                return true;  // stop searching
            }
        });

        await db
            .collection('projects')
            .findOneAndReplace({ slug: project.slug }, project);
        
        res.json({ board });
    }
});

module.exports = projectsRouter