import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  test('DELETE  /projects/:slug deletes a matching project', async () => {
    // Creates 'awesome' project
    const project = { name: 'Awesome' };
    await request(app).post('/projects').send(project);

    // Deletes 'awesome' project
    const name = 'awesome';
    const { status } = await request(app).delete(`/projects`).send({ name });
    
    // Check if 'awesome' project was deleted
    const { body } = await request(app).get('/projects/awesome');

    expect(status).toBe(200);
    expect(body.error).toBeDefined();
  });

  test('DELETE  /projects/:slug/boards/:name deletes a matching board', async () => {
    // Creates 'awesome' project and 'todo' board
    const project = { name: 'Awesome' };
    await request(app).post('/projects').send(project);
    const name = 'todo';
    await request(app).post(`/projects/awesome/boards`).send({ name });

    // Deletes 'todo' board
    const { status } = await request(app).delete(`/projects/awesome/boards`).send({ name });
    
    // Check if 'todo' board was deleted
    const { body } = await request(app).get('/projects/awesome');

    expect(status).toBe(200);
    expect(body.project.boards).toMatchObject([]);
  });

  test('POST /projects/:slug/boards/:name/tasks adds a new task to a board', async () => {
    // Creates 'awesome' project and 'todo' board
    const project = { name: 'Awesome' };
    await request(app).post('/projects').send(project);
    await request(app).post(`/projects/awesome/boards`).send({ name: 'todo' });

    // Add task to 'todo' board
    const name = '1. do';
    const { body } = await request(app).post(`/projects/awesome/boards/todo/tasks`).send({ name });

    expect(body.board.tasks).toBeDefined();
    expect(body.board.tasks[0]).toBe(name);
  });

  test('DELETE /projects/:slug/boards/name/tasks/:id deletes a matching task', async () => {
    // Creates 'awesome' project, 'todo' board and '1.do' task
    const project = { name: 'Awesome' };
    await request(app).post('/projects').send(project);
    await request(app).post(`/projects/awesome/boards`).send({ name: 'todo' });
    const name = '1. do';
    await request(app).post(`/projects/awesome/boards/todo/tasks`).send({ name });

    // Deletes task
    const { status } = await request(app).delete(`/projects/awesome/boards/todo/tasks`).send({ name });

    // Check if task was deleted
    const { body } = await request(app).get('/projects/awesome');

    expect(status).toBe(200);
    expect(body.project.boards[0].tasks).toMatchObject([]);
  });
});
